import {
  GetProductsForIngredient,
  GetRecipes,
} from './supporting-files/data-access';
import {
  NutrientFact,
  Product,
  SupplierProduct,
} from './supporting-files/models';
import {
  ConvertUnits,
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from './supporting-files/helpers';
import { RunTest, ExpectedRecipeSummary } from './supporting-files/testing';

console.clear();
console.log('Expected Result Is:', ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

recipeData.forEach((recipe) => {
  // console.log(recipe.lineItems.length);
  var cheapestCostAmount = 0;
  let nutrientsAtCheapestCost: any = {};
  recipe.lineItems.forEach((item) => {
    const products: Product[] = GetProductsForIngredient(item.ingredient);
    let cheaperValues: number[] = [];
    let cheaperAmount: number[] = [];
    products.map((product) => {
      if (product.supplierProducts.length > 0) {
        product.supplierProducts.sort(
          (firstProduct, otherProduct) =>
            GetCostPerBaseUnit(firstProduct) - GetCostPerBaseUnit(otherProduct)
        );
        const unit = ConvertUnits(
          item.unitOfMeasure,
          product.supplierProducts[0].supplierProductUoM.uomName,
          product.supplierProducts[0].supplierProductUoM.uomType
        );
        cheaperAmount.push(GetCostPerBaseUnit(product.supplierProducts[0]));
        cheaperValues.push(
          GetCostPerBaseUnit(product.supplierProducts[0]) * unit.uomAmount
        );
      }
    });
    const product = products[cheaperValues.indexOf(Math.min(...cheaperValues))];
    cheapestCostAmount += Math.min(...cheaperValues);
    product.nutrientFacts.forEach((item) => {
      nutrientsAtCheapestCost = {
        ...nutrientsAtCheapestCost,
        [item.nutrientName]: GetNutrientFactInBaseUnits(item),
      };
      // console.log(GetNutrientFactInBaseUnits(item));
    });
  });
  recipeSummary[recipe.recipeName] = {
    cheapestCost: cheapestCostAmount,
    nutrientsAtCheapestCost: nutrientsAtCheapestCost,
  };
});

console.log(recipeSummary);
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
